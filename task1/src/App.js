import * as React from "react";
import Button from "@mui/material/Button";
import { IconButton } from "@mui/material";
import { TextField } from "@mui/material";
import EmailIcon from "@mui/icons-material/Email";
import LockIcon from "@mui/icons-material/Lock";
import { FormControlLabel } from "@mui/material";
import { Checkbox } from "@mui/material";
import image from "./pic1.png";
import TwitterIcon from "@mui/icons-material/Twitter";
import FacebookIcon from "@mui/icons-material/Facebook";
import GoogleIcon from "@mui/icons-material/Google";
import ButtonGroup from "@mui/material/ButtonGroup";
import "./App.scss";

function App() {
  return (
    <div className="login">
      <div className="title"> LOGIN PAGE</div>
      <img src={image} alt="" height="180vh"></img>
      <div className="data">
        <IconButton color="primary" size="large">
          <EmailIcon />
        </IconButton>
        <TextField
          id="outlined-basic"
          label="Enter valid E-mail"
          variant="outlined"
        />

        <IconButton color="primary" size="large">
          <LockIcon />
        </IconButton>
        <TextField
          id="outlined-basic"
          label="Enter valid Password"
          variant="outlined"
        />
      </div>
      <div>
        <FormControlLabel
          control={<Checkbox defaultChecked />}
          label=" Remember Me"
        />
       <div className="theory">
       Forgot password?
       
       </div>
      </div>
      <div className="login-button">
        <Button sx={{ width: "200px" }} variant="contained">
          Login
        </Button>
      </div>
      <div className="symbols">
          <div className="text"> Or Sign In using</div>
        <div >
          <ButtonGroup variant="text" aria-label="text button group">
            <Button>
              <GoogleIcon />{" "}
            </Button>
            <Button>
              {" "}
              <TwitterIcon />
            </Button>
            <Button>
              <FacebookIcon />
            </Button>
          </ButtonGroup>
        </div>
      </div>
    </div>
  );
}

export default App;
